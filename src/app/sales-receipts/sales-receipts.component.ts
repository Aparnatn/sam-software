import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sales-receipts',
  templateUrl: './sales-receipts.component.html',
  styleUrls: ['./sales-receipts.component.scss']
})
export class SalesReceiptsComponent implements OnInit {

  constructor(private router:Router,) { }

  ngOnInit(): void {
  }
  onSubmit1(): void {

    this.router.navigate(['/sales']);
  }
}

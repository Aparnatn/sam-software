import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-purchase-receipts',
  templateUrl: './purchase-receipts.component.html',
  styleUrls: ['./purchase-receipts.component.scss']
})
export class PurchaseReceiptsComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onSubmit1(): void {

    this.router.navigate(['/purchase']);
  }
}

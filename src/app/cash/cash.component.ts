import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { SalesService } from '../services/sales.service';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-cash',
  templateUrl: './cash.component.html',
  styleUrls: ['./cash.component.scss']
})
export class CashComponent implements OnInit {

  CashSaleForm = this.formBuilder.group({
    invoice_number: '',
    date: '',
    internal_ref_no:'' ,
    cash: '',
    user_id: '',
    account: '',
    customer_id: '',
    customer_name:'',
    item_id1: '',
    item_id2: '',
    item_details1: '',
    item_details2: '',
    price1_1: '',
    price1_2:'' ,

    quantity1: '',
    quantity2: '',
    amount1: '',
    amount2: '',
    sales_ex1: '',
    sales_ex2:'' ,
    job1: '',
    job2:'' ,
    labour_charge: '',
    other_charge: '',
    total1: '',
    total2: '',
    total3: '',

    discount:'' ,


  });

  constructor(private http:HttpClient,private router:Router,private service:SalesService,private formBuilder: FormBuilder) {}





  ngOnInit(): void {
    const headers = new Headers();
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*');
    this.http.post("http://127.0.0.1:8004/Sam/cashapi", {headers: headers}).subscribe(res => {

      console.log(res);
  });

  }

  onSubmit1(): void {


this.service.cashSale(this.CashSaleForm.value,).subscribe((data,)=>{
console.log(data);

  });
}
}

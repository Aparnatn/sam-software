import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { LoginRequest, LoginResponse } from './login.interfaces';

@Injectable()
export class UserService
{
    private apiUrl: string;

    constructor(private http: HttpClient) { 
        this.apiUrl = environment.apiUrl
    }

    login(data: LoginRequest ): Observable<LoginResponse> {
        return this.http.post<LoginResponse>(`${this.apiUrl}/login`, data, {
            // observe: 'response',
            withCredentials: true
        });
    }
}
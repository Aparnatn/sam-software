import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CashSaleRequest, CashSaleResponse, CreditPurchaseRequest, CreditPurchaseResponse, CreditSaleRequest, CreditSaleResponse, PCashSaleRequest, PCashSaleResponse } from '../interfaces/sales.interfaces';


@Injectable()
export class SalesService
{
    private apiUrl: string;

    constructor(private http: HttpClient) {
        this.apiUrl = environment.apiUrl
    }

    cashSale(data: CashSaleRequest ): Observable<CashSaleResponse> {
        return this.http.post<CashSaleResponse>(`${this.apiUrl}/Sam/cashapi`, data, {
            // observe: 'response',
            withCredentials: true
        });

    }
    PcashSale(data: PCashSaleRequest ): Observable<PCashSaleResponse> {
      return this.http.post<CashSaleResponse>(`${this.apiUrl}/Sam/PCashApi`, data, {
          // observe: 'response',
          withCredentials: true
      });
    }
    creditPurchase(data: CreditPurchaseRequest ): Observable<CreditPurchaseResponse> {
      return this.http.post<CashSaleResponse>(`${this.apiUrl}/Sam/PCreditApi`, data, {
          // observe: 'response',
          withCredentials: true
      });
    }
    creditSale(data: CreditSaleRequest ): Observable<CreditSaleResponse> {
      return this.http.post<CashSaleResponse>(`${this.apiUrl}/Sam/creditapi`, data, {
          // observe: 'response',
          withCredentials: true
      });
    }
}

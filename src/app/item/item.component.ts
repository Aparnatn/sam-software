import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  constructor(private router:Router,) { }

  ngOnInit(): void {
  }
  
  onSubmit1(): void {

    this.router.navigate(['/grand-hyper']);
  }
  onSubmit2(): void {

    this.router.navigate(['/grand-hyper']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-grand-hyper',
  templateUrl: './grand-hyper.component.html',
  styleUrls: ['./grand-hyper.component.scss']
})
export class GrandHyperComponent implements OnInit {

  constructor(private router:Router,) { }

  ngOnInit(): void {
  }
  onSubmit1(): void {

    this.router.navigate(['/sales']);
  }
  onSubmit2(): void {

    this.router.navigate(['/journal-entry']);
  }
  onSubmit3(): void {

    this.router.navigate(['/register']);
  }
  onSubmit4(): void {

    this.router.navigate(['/reports']);
  }
  onSubmit5(): void {

    this.router.navigate(['/payroll']);
  }
  onSubmit6(): void {

    this.router.navigate(['/purchase']);
  }
}
